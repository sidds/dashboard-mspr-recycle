## Java Spring Boot project (jdk 8)

Frontend de notre projet mspr réalisé en Java avec le framework Spring Boot ainsi que Maven en package manager.
Il communique avec une API NodeJs, pour les besoins de ce projet nous avons réalisé des tests unitaires avec Junit permettant de tester ce service externe.

## CI/CD 

#### Notre CI est composé de deux parties:
* Test : job qui lance les tests unitaires du projets.
* Build : Compilation et création d'un fichier .jar téléchargeable depuis la pipeline.

#### Notre CD est composé de 3 parties:
* Docker : build l'image docker de notre projet à l'aide du Dockerfile à la racine et push le container sur le gitlab docker registry du projet.
* Push Lastest : Recupère la dernière image push lui ajoute le tag latest puis la push de nouveau sur le registry, celà permet de récupérer ^plus facilement la dernière version.
* Push Tag (optionnel) : Si un tag est spécifié pour la branch alors le tag est ajouté à l'image puis renvoyer dans le registry


> Ce projet à été réalisé par CHAMBON Nicolas, DJILALI SAIAH Sid, BASSIGNANI Théo, DESLSOL Antonin, TRAN Delphine