package fr.epsi.recyclr;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import java.io.IOException;

import static org.junit.Assert.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RecycleServicesTests {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private String bearer = null;
    private String userToken = null;

    public String getBearer() {
        return bearer;
    }

    public void setBearer(String bearer) {
        this.bearer = bearer;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }



    @BeforeAll
    public void initHeader() throws IOException, JSONException {
        String loginUrl = "http://35.233.91.152:3000/login";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject personJsonObject = new JSONObject();
        personJsonObject.put("login", "D_GCHA");
        personJsonObject.put("password", "123456789");
        HttpEntity<String> request = new HttpEntity<String>(personJsonObject.toString(), headers);

        ResponseEntity<String> responseEntityStr = restTemplate.postForEntity(loginUrl, request, String.class);
        JsonNode body = objectMapper.readTree(responseEntityStr.getBody());

        setBearer(body.path("bearer").asText());
        setUserToken(body.path("data").path("TOKEN").asText());
    }

    @Test
    public void loginRouteTest() throws IOException, JSONException {
        String loginUrl = "http://35.233.91.152:3000/login";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject personJsonObject = new JSONObject();
        personJsonObject.put("login", "D_GCHA");
        personJsonObject.put("password", "123456789");
        HttpEntity<String> request = new HttpEntity<String>(personJsonObject.toString(), headers);

        ResponseEntity<String> responseEntityStr = restTemplate.postForEntity(loginUrl, request, String.class);
        JsonNode body = objectMapper.readTree(responseEntityStr.getBody());

        assertNotNull(responseEntityStr);
        assertEquals(responseEntityStr.getStatusCode(), HttpStatus.OK);
        assertTrue(body.path("bearer").asText().contains("Bearer"));
        assertNotNull(body.path("data").path("TOKEN"));
    }

    @Test
    public void demandesRouteTest() throws IOException {
        String demandeUrl = "http://35.233.91.152:3000/demande/1";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("authorization", getBearer());
        headers.set("usertoken", getUserToken());
        HttpEntity<String> request = new HttpEntity<>(null, headers);

        ResponseEntity<String> responseEntityStr = restTemplate.exchange(demandeUrl, HttpMethod.GET, request, String.class);
        JsonNode body = objectMapper.readTree(responseEntityStr.getBody());

        assertNotNull(responseEntityStr);
        assertEquals(responseEntityStr.getStatusCode(), HttpStatus.OK);
        assertEquals(body.path("NODEMANDE").asText(), "1");
        assertEquals(body.path("RAISONSOCIALE").asText(), "Decathlon");
    }
}
