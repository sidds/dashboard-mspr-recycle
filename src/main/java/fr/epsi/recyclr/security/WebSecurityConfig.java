//package fr.epsi.recyclr.security;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//@Configuration
//@EnableWebSecurity
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//    private static final String ENCODED_PASSWORD = "123";
//
////    @Autowired
////    private JwtEntryPoint jwtAuthenticationEntryPoint;
////
////    @Autowired
////    private UserDetailsService jwtUserDetailsService;
////
////    @Autowired
////    private JwtRequestFilter jwtRequestFilter;
////
////    @Autowired
////    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
////        // configure AuthenticationManager so that it knows from where to load
////        // user for matching credentials
////        // Use BCryptPasswordEncoder
////        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
////    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers("/webjars/**", "/css/**", "/img/**").permitAll()
//                .antMatchers("/favicon.ico").permitAll()
//                .antMatchers("/forgot_password").permitAll()
//                .anyRequest().authenticated()
//                .and()
//                .formLogin()
//                .loginPage("/login")
//                .loginProcessingUrl("/authuser")
//                .defaultSuccessUrl("/", true)
//                .permitAll()
//                .and()
//                .logout()
//                .permitAll();
//    }
//
////    @Override
////    protected void configure(HttpSecurity httpSecurity) throws Exception {
////        // We don't need CSRF for this example
////        httpSecurity.csrf().disable()
////                // dont authenticate this particular request
////                .authorizeRequests()
////                .antMatchers("/authenticate").permitAll()
////                .antMatchers("/webjars/**", "/css/**", "/img/**").permitAll()
////                .antMatchers("/favicon.ico").permitAll()
////                .antMatchers("/forgot_password").permitAll()
////                // all other requests need to be authenticated
////                .anyRequest().authenticated()
////                .and()
////                .logout().permitAll()
////                .and()
////                // make sure we use stateless session; session won't be used to
////                // store user's state.
////                .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
////                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
////
////        // Add a filter to validate the tokens with every request
////        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
////    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .passwordEncoder(passwordEncoder())
//                .withUser("user").password(passwordEncoder().encode(ENCODED_PASSWORD)).roles("USER");
//    }
//
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    @Bean
//    @Override
//    public AuthenticationManager authenticationManagerBean() throws Exception {
//        return super.authenticationManagerBean();
//    }
//}
