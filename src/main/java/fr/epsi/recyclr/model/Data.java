package fr.epsi.recyclr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public class Data {

    @JsonProperty("NOEMPLOYE")
    private int noEmploye;
    @JsonProperty("LOGIN")
    private String login;
    @JsonProperty("PASS")
    private String password;
    @JsonProperty("NOM")
    private String lastName;
    @JsonProperty("PRENOM")
    private String firstName;
    @JsonProperty("DATENAISS")
    private String dateNaissance;
    @JsonProperty("DATAEMBAUCHE")
    private String dateEmbauche;
    @JsonProperty("DISPONIBILITE")
    private String disponibilite;
    @JsonProperty("EMAIL")
    private String email;
    @JsonProperty("COUNT(\"TOURNEE\".\"NOEMPLOYE\")")
    private int nbTournee;
    @JsonProperty("NOMSITE")
    private String nomSite;
    @JsonProperty("NOMFONCTION")
    private String nomFonction;
    @JsonProperty("TOKEN")
    private String userToken;

    public Data() {
    }

    @Override
    public String toString() {
        return "Data{" +
                "noEmploye=" + noEmploye +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", dateNaissance='" + dateNaissance + '\'' +
                ", dateEmbauche='" + dateEmbauche + '\'' +
                ", disponibilite='" + disponibilite + '\'' +
                ", email='" + email + '\'' +
                ", nbTournee=" + nbTournee +
                ", nomSite='" + nomSite + '\'' +
                ", nomFonction='" + nomFonction + '\'' +
                ", userToken='" + userToken + '\'' +
                '}';
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getNomSite() {
        return nomSite;
    }

    public void setNomSite(String nomSite) {
        this.nomSite = nomSite;
    }

    public String getNomFonction() {
        return nomFonction;
    }

    public void setNomFonction(String nomFonction) {
        this.nomFonction = nomFonction;
    }

    public int getNbTournee() {
        return nbTournee;
    }

    public void setNbTournee(int nbTournee) {
        this.nbTournee = nbTournee;
    }

    public int getNoEmploye() {
        return noEmploye;
    }

    public void setNoEmploye(int noEmploye) {
        this.noEmploye = noEmploye;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(String dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    public String getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilite(String disponibilite) {
        this.disponibilite = disponibilite;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
