package fr.epsi.recyclr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Site {

    @JsonProperty("NOSITE")
    private int noSite;
    @JsonProperty("NOMSITE")
    private String nomSite;

    public int getNoSite() {
        return noSite;
    }

    public void setNoSite(int noSite) {
        this.noSite = noSite;
    }

    public String getNomSite() {
        return nomSite;
    }

    public void setNomSite(String nomSite) {
        this.nomSite = nomSite;
    }

    @Override
    public String toString() {
        return "Site{" +
                "noSite=" + noSite +
                ", nomSite='" + nomSite + '\'' +
                '}';
    }
}
