package fr.epsi.recyclr.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DetailRequest {

    @JsonProperty("NODEMANDE")
    private int noDemande;
    @JsonProperty("QUANTITEENLEVEE")
    private int quantiteEnlevee;
    @JsonProperty("QUANTITEDEMANDE")
    private int quantiteDemande;
    @JsonProperty("REMARQUE")
    private String remarque;
    @JsonProperty("NOTYPEDECHET")
    private int noTypeDechet;
    @JsonProperty("NOMTYPEDECHET")
    private String typeDechet;
    @JsonProperty("NOTOURNEE")
    private String noTournee;

    public DetailRequest(){

    }

    public int getNoTypeDechet() {
        return noTypeDechet;
    }

    public void setNoTypeDechet(int noTypeDechet) {
        this.noTypeDechet = noTypeDechet;
    }

    public int getNoDemande() {
        return noDemande;
    }

    public void setNoDemande(int noDemande) {
        this.noDemande = noDemande;
    }

    public int getQuantiteEnlevee() {
        return quantiteEnlevee;
    }

    public void setQuantiteEnlevee(int quantiteEnlevee) {
        this.quantiteEnlevee = quantiteEnlevee;
    }

    public int getQuantiteDemande() {
        return quantiteDemande;
    }

    public void setQuantiteDemande(int quantiteDemande) {
        this.quantiteDemande = quantiteDemande;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public String getTypeDechet() {
        return typeDechet;
    }

    public void setTypeDechet(String typeDechet) {
        this.typeDechet = typeDechet;
    }

    public String getNoTournee() {
        return noTournee;
    }

    public void setNoTournee(String noTournee) {
        this.noTournee = noTournee;
    }

    @Override
    public String toString() {
        return "DetailRequest{" +
                "noDemande=" + noDemande +
                ", quantiteEnlevee=" + quantiteEnlevee +
                ", quantiteDemande=" + quantiteDemande +
                ", remarque='" + remarque + '\'' +
                ", noTypeDechet=" + noTypeDechet +
                ", typeDechet='" + typeDechet + '\'' +
                ", noTournee='" + noTournee + '\'' +
                '}';
    }
}
