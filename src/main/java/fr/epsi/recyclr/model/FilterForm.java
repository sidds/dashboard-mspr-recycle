package fr.epsi.recyclr.model;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class FilterForm implements Serializable {
    @NotBlank
    private String dateFrom;
    @NotBlank
    private String dateTo;
    @NotBlank
    private String filterChoice;
    private String site;
    @NotBlank
    private String typeDechet;

    public FilterForm() {
    }

    public String getFilterChoice() {
        return filterChoice;
    }

    public void setFilterChoice(String filterChoice) {
        this.filterChoice = filterChoice;
    }

    public String getTypeDechet() {
        return typeDechet;
    }

    public void setTypeDechet(String typeDechet) {
        this.typeDechet = typeDechet;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public String toString() {
        return "FilterForm{" +
                "dateFrom='" + dateFrom + '\'' +
                ", dateTo='" + dateTo + '\'' +
                ", filterChoice='" + filterChoice + '\'' +
                ", site='" + site + '\'' +
                ", typeDechet='" + typeDechet + '\'' +
                '}';
    }
}
