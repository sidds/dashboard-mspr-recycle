package fr.epsi.recyclr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public class TrashCan {
    @JsonProperty("DEVICE_ID")
    private String deviceId;
    @JsonProperty("NOID")
    private String noId;
    @JsonProperty("DATE_RELEVE")
    private String dateReleve;
    @JsonProperty("FILLLEVEL")
    private float fillLevel;
    @JsonProperty("ODORMETER")
    private float odorMeter;
    @JsonProperty("WEIGHT")
    private String weight;

    public TrashCan() {
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getNoId() {
        return noId;
    }

    public void setNoId(String noId) {
        this.noId = noId;
    }

    public String getDateReleve() {
        return dateReleve;
    }

    public void setDateReleve(String dateReleve) {
        this.dateReleve = dateReleve;
    }

    public float getFillLevel() {
        return fillLevel;
    }

    public void setFillLevel(float fillLevel) {
        this.fillLevel = fillLevel;
    }

    public float getOdorMeter() {
        return odorMeter;
    }

    public void setOdorMeter(float odorMeter) {
        this.odorMeter = odorMeter;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "TrashCan{" +
                "deviceId='" + deviceId + '\'' +
                ", noId='" + noId + '\'' +
                ", dateReleve='" + dateReleve + '\'' +
                ", fillLevel=" + fillLevel +
                ", odorMeter=" + odorMeter +
                ", weight='" + weight + '\'' +
                '}';
    }
}
