package fr.epsi.recyclr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.Date;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {

    @JsonProperty("DATEENLEVEMENT")
    private Date DATEENLEVEMENT;
    @JsonProperty("DATEDEMANDE")
    private Date DATEDEMANDE;
    @JsonProperty("SIRET")
    private String SIRET;
    @JsonProperty("NOPOUBELLE")
    private String NOPOUBELLE;
    @JsonProperty("NOSITE")
    private int NOSITE;
    @JsonProperty("NODEMANDE")
    private int noDemande;
    @JsonProperty("VILLESITE")
    private String villeSite;
    @JsonProperty("DETAILDEMANDES")
    @JsonDeserialize(as = ArrayList.class, contentAs = DetailRequest.class)
    private ArrayList<DetailRequest> detailsRequest;
    @JsonProperty("CONTACT")
    private String contact;
    @JsonProperty("NOTEL")
    private  String noTel;
    @JsonProperty("RAISONSOCIALE")
    private String raisonSociale;




    public Request(){
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getNoTel() {
        return noTel;
    }

    public void setNoTel(String noTel) {
        this.noTel = noTel;
    }

    public Date getDATEENLEVEMENT() {
        return DATEENLEVEMENT;
    }

    public void setDATEENLEVEMENT(Date DATEENLEVEMENT) {
        this.DATEENLEVEMENT = DATEENLEVEMENT;
    }

    public Date getDATEDEMANDE() {
        return DATEDEMANDE;
    }

    public void setDATEDEMANDE(Date DATEDEMANDE) {
        this.DATEDEMANDE = DATEDEMANDE;
    }

    public String getSIRET() {
        return SIRET;
    }

    public void setSIRET(String SIRET) {
        this.SIRET = SIRET;
    }

    public String getNOPOUBELLE() {
        return NOPOUBELLE;
    }

    public void setNOPOUBELLE(String NOPOUBELLE) {
        this.NOPOUBELLE = NOPOUBELLE;
    }

    public int getNOSITE() {
        return NOSITE;
    }

    public void setNOSITE(int NOSITE) {
        this.NOSITE = NOSITE;
    }

    public int getNoDemande() {
        return noDemande;
    }

    public void setNoDemande(int noDemande) {
        this.noDemande = noDemande;
    }

    public String getVilleSite() {
        return villeSite;
    }

    public void setVilleSite(String villeSite) {
        this.villeSite = villeSite;
    }

    public ArrayList<DetailRequest> getDetailsRequest() {
        return detailsRequest;
    }

    public void setDetailsRequest(ArrayList<DetailRequest> detailsRequest) {
        this.detailsRequest = detailsRequest;
    }

    @Override
    public String toString() {
        return "Request{" +
                "DATEENLEVEMENT='" + DATEENLEVEMENT + '\'' +
                ", DATEDEMANDE='" + DATEDEMANDE + '\'' +
                ", SIRET='" + SIRET + '\'' +
                ", NOPOUBELLE='" + NOPOUBELLE + '\'' +
                ", NOSITE=" + NOSITE +
                ", noDemande=" + noDemande +
                ", villeSite='" + villeSite + '\'' +
                ", detailDemandes=" + detailsRequest +
                ", contact='" + contact + '\'' +
                ", noTel='" + noTel + '\'' +
                '}';
    }


}
