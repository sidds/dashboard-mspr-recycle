package fr.epsi.recyclr.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Waste {

    @JsonProperty("NOTYPEDECHET")
    private int noTypeDechet;
    @JsonProperty("NOMTYPEDECHET")
    private String typeDechet;
    @JsonProperty("SUM(\"QUANTITEENLEVEE\")")
    private int quantiteEnleveee;

    public Waste() {
    }

    public int getNoTypeDechet() {
        return noTypeDechet;
    }

    public void setNoTypeDechet(int noTypeDechet) {
        this.noTypeDechet = noTypeDechet;
    }

    public String getTypeDechet() {
        return typeDechet;
    }

    public void setTypeDechet(String typeDechet) {
        this.typeDechet = typeDechet;
    }

    public int getQuantiteEnleveee() {
        return quantiteEnleveee;
    }

    public void setQuantiteEnleveee(int quantiteEnleveee) {
        this.quantiteEnleveee = quantiteEnleveee;
    }

    @Override
    public String toString() {
        return "Waste{" +
                "noTypeDechet=" + noTypeDechet +
                ", typeDechet='" + typeDechet + '\'' +
                ", quantiteEnleveee=" + quantiteEnleveee +
                '}';
    }
}
