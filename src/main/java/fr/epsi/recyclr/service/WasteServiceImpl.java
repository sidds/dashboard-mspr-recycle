package fr.epsi.recyclr.service;

import fr.epsi.recyclr.config.ApiAccess;
import fr.epsi.recyclr.model.Waste;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.*;

@Service
public class WasteServiceImpl implements WasteService {
    @Override
    public List<Waste> getPickedUpWaste(HttpSession session, String dateFrom, String dateTo, String site, String typeDechet) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = ApiAccess.setRequestHeader(session);
        Map<String, Object> map = new HashMap<>();
        map.put("dateFrom", dateFrom);
        map.put("dateTo", dateTo);
        // build the request
        if (!site.isEmpty()) {
            map.put("id", site);
        }
        boolean verifyTypeDechetParameter = !typeDechet.isEmpty() && !typeDechet.equals("empty") && !typeDechet.equals("unknown");
        if (verifyTypeDechetParameter) {
            map.put("id", typeDechet);
        }
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        // send POST request
        ResponseEntity<Waste[]> responseEntity;
        if (verifyTypeDechetParameter) {
            responseEntity = restTemplate.postForEntity(ApiAccess.getUrl() + "sumbytypedechet", entity, Waste[].class);
        } else if (!site.isEmpty()) {
            responseEntity = restTemplate.postForEntity(ApiAccess.getUrl() + "sum-type-dechet-by-site", entity, Waste[].class);
        } else {
            responseEntity = restTemplate.postForEntity(ApiAccess.getUrl() + "sumtypedechet", entity, Waste[].class);
        }
        return checkResponseBody(responseEntity);
    }

    @Override
    public List<Waste> getAllWasteType(HttpSession session) {
        HttpHeaders headers = ApiAccess.setRequestHeader(session);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<Waste[]> responseEntity = restTemplate.exchange(ApiAccess.getUrl() + "all-type-dechet", HttpMethod.GET, request, Waste[].class);
        return checkResponseBody(responseEntity);
    }

    private List<Waste> checkResponseBody(ResponseEntity<Waste[]> responseEntity) {
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            if (responseEntity.getBody() != null) {
                return Arrays.asList(responseEntity.getBody());

            } else {
                return new ArrayList<>();
            }

        } else {
            System.out.println("Request Failed");
            return new ArrayList<>();
        }
    }
}
