package fr.epsi.recyclr.service;

import fr.epsi.recyclr.model.TrashCan;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface PoubelleService {
    public abstract List<TrashCan> getTrashCansInformation(HttpSession session);
}
