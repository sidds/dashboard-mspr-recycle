package fr.epsi.recyclr.service;

import fr.epsi.recyclr.model.Data;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface EmployeeService {
    public abstract List<Data> getAllEmployees(HttpSession session);
    public abstract List<Data> getAllEmployeesNbTournee(HttpSession session);
}
