package fr.epsi.recyclr.service;

import fr.epsi.recyclr.config.ApiAccess;
import fr.epsi.recyclr.model.Data;
import fr.epsi.recyclr.model.TrashCan;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PoubellesServiceImpl implements PoubelleService {
    @Override
    public List<TrashCan> getTrashCansInformation(HttpSession session) {
        HttpHeaders headers = ApiAccess.setRequestHeader(session);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<TrashCan[]> response = restTemplate.exchange(ApiAccess.getUrl() + "get-record-poubelles", HttpMethod.GET, request, TrashCan[].class);
        if (response.getStatusCode() == HttpStatus.OK) {
            System.out.println("Le body: "+ response.getBody());
            return Arrays.asList(response.getBody());
        } else {
            System.out.println("Request Failed");
            return new ArrayList<>();
        }
    }
}
