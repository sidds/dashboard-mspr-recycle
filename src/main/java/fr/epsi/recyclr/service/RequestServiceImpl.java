package fr.epsi.recyclr.service;

import fr.epsi.recyclr.config.ApiAccess;
import fr.epsi.recyclr.model.DetailRequest;
import fr.epsi.recyclr.model.Employee;
import fr.epsi.recyclr.model.Request;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.*;

@Service
public class RequestServiceImpl implements RequestService {

    private static RestTemplate restTemplate = new RestTemplate();
    private static HttpHeaders headers = new HttpHeaders();

    @Override
    public List<Request> getAllRequests(HttpSession session) {

        HttpHeaders headers = ApiAccess.setRequestHeader(session);
        HttpEntity request = new HttpEntity(headers);
        // make an HTTP GET request with headers
        ResponseEntity<Request[]> response = restTemplate.exchange(
                ApiAccess.getUrl()+"/demandes",
                HttpMethod.GET,
                request,
                Request[].class
        );
        // check response
        if (response.getStatusCode() == HttpStatus.OK) {
            return Arrays.asList(response.getBody());
        } else {
            System.out.println("Request Failed");
            return new ArrayList<>();
        }
    }

    @Override
    public Request getRequest(HttpSession session, String id) {
        headers = ApiAccess.setRequestHeader(session);
        // build the request
        HttpEntity request = new HttpEntity(headers);
        // make an HTTP GET request with headers
        ResponseEntity<Request> response = restTemplate.exchange(
                ApiAccess.getUrl()+"/demande/"+id,
                HttpMethod.GET,
                request,
                Request.class
        );
        // check response
        if (response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        } else {
            return new Request();
        }
    }

    @Override
    public List<DetailRequest> getAllRequestsNoSubmit(HttpSession session) {
        HttpHeaders headers = ApiAccess.setRequestHeader(session);

        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<DetailRequest[]> response = restTemplate.exchange(
                ApiAccess.getUrl()+"demandesnosubmit",
                HttpMethod.GET,
                request,
                DetailRequest[].class
        );
        if (response.getStatusCode() == HttpStatus.OK){
            return Arrays.asList(response.getBody());
        }else {
            return new ArrayList<>();
        }
    }
}
