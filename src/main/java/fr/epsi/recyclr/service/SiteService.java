package fr.epsi.recyclr.service;

import fr.epsi.recyclr.model.Site;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface SiteService {
    public abstract List<Site> getAllSites(HttpSession session);
}
