package fr.epsi.recyclr.service;

import fr.epsi.recyclr.model.Waste;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

public interface WasteService {
    public abstract List<Waste> getPickedUpWaste(HttpSession session, String dateFrom, String dateTo, String site, String typeDechet);
    public abstract List<Waste> getAllWasteType(HttpSession session);
}
