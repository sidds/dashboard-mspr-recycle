package fr.epsi.recyclr.service;

import fr.epsi.recyclr.model.DetailRequest;
import fr.epsi.recyclr.model.Request;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface RequestService {
    public abstract List<Request> getAllRequests(HttpSession session);
    public abstract Request getRequest(HttpSession session, String id);
    public abstract List<DetailRequest> getAllRequestsNoSubmit(HttpSession session);
}
