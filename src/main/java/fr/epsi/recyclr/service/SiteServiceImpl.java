package fr.epsi.recyclr.service;

import fr.epsi.recyclr.config.ApiAccess;
import fr.epsi.recyclr.model.Site;
import fr.epsi.recyclr.model.Waste;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class SiteServiceImpl implements SiteService {

    @Override
    public List<Site> getAllSites(HttpSession session) {
        HttpHeaders headers = ApiAccess.setRequestHeader(session);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<Site[]> responseEntity = restTemplate.exchange(ApiAccess.getUrl() + "all-sites", HttpMethod.GET, request, Site[].class);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            if (responseEntity.getBody() != null) {
                return Arrays.asList(responseEntity.getBody());

            } else {
                return new ArrayList<>();
            }

        } else {
            System.out.println("Request Failed");
            return new ArrayList<>();
        }
    }
}
