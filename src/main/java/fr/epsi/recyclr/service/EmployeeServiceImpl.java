package fr.epsi.recyclr.service;

import fr.epsi.recyclr.config.ApiAccess;
import fr.epsi.recyclr.model.Data;
import fr.epsi.recyclr.model.Employee;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Override
    public List<Data> getAllEmployees(HttpSession session) {
        HttpHeaders headers = ApiAccess.setRequestHeader(session);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<Data[]> response = restTemplate.exchange(ApiAccess.getUrl() + "utilisateurs", HttpMethod.GET, request, Data[].class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return Arrays.asList(response.getBody());
        } else {
            System.out.println("Request Failed");
            return new ArrayList<>();
        }
    }

    @Override
    public List<Data> getAllEmployeesNbTournee(HttpSession session) {
        HttpHeaders headers = ApiAccess.setRequestHeader(session);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<Data[]> response = restTemplate.exchange(ApiAccess.getUrl() + "nbtourneebyemployee", HttpMethod.GET, request, Data[].class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return Arrays.asList(response.getBody());
        } else {
            System.out.println("Request Failed");
            return new ArrayList<>();
        }
    }
}
