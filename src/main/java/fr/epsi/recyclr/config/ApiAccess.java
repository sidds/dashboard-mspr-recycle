package fr.epsi.recyclr.config;

import fr.epsi.recyclr.model.Employee;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;

public class ApiAccess {

    private static final String url = "http://35.233.91.152:3000/";

    public static String getUrl() {
        return url;
    }

    public static HttpHeaders setRequestHeader(HttpSession session){
        HttpHeaders headers = new HttpHeaders();
        Employee employee = (Employee)session.getAttribute("employee");
        String jwtToken = employee.getToken();
        String userToken = employee.getData().getUserToken();
        System.out.println("userToken = " + userToken);
        // add user-agent
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0");
        headers.put("authorization", new ArrayList<>(Collections.singleton(jwtToken)));
        headers.put("usertoken", new ArrayList<>(Collections.singleton(userToken)));
        // build the request
        return headers;
    }
}
