package fr.epsi.recyclr.config;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component
public class CustomFilter implements Filter {
    public static final String ACCES_PUBLIC = "/login";
    public static final String ATT_SESSION_USER = "employee";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        /* Non-filtrage des ressources statiques */
        String chemin = httpServletRequest.getRequestURI().substring( httpServletRequest.getContextPath().length());
        if (chemin.contains("/authuser") || chemin.contains("/forgot_password") || chemin.contains("/forgot") || chemin.contains("/reset_password") || chemin.contains("/reset-password") || chemin.contains("/reset_session")) {
            chain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        //     Donne accès au ressource
        if ( chemin.startsWith( "/webjars") || chemin.contains("/img") || chemin.startsWith("/css") || chemin.startsWith("/js") || chemin.startsWith("/favicon")) {
            chain.doFilter( httpServletRequest, httpServletResponse );
            return;
        }

        HttpSession session = httpServletRequest.getSession();
        if(session.getAttribute(ATT_SESSION_USER) == null){
            request.getRequestDispatcher(ACCES_PUBLIC).forward(request, response);
        }else {
            chain.doFilter(httpServletRequest, httpServletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
