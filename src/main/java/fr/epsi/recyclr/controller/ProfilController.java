package fr.epsi.recyclr.controller;


import fr.epsi.recyclr.model.Data;
import fr.epsi.recyclr.model.Employee;
import fr.epsi.recyclr.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/profil")
public class ProfilController {
    @Autowired
    EmployeeService employeeService;

    @GetMapping("")
    public String profil(HttpSession session, Model model) {
        Employee employee = (Employee) session.getAttribute("employee");
        System.out.println("employee = " + employee.getData());
        List<Data> employeeList = employeeService.getAllEmployeesNbTournee(session);
        int nbTournee = 0;
        for (Data data: employeeList
             ) {
            if(data.getNoEmploye() == employee.getData().getNoEmploye()){
                nbTournee = data.getNbTournee();
            }
        }
        model.addAttribute("nbTournee", nbTournee);
        return "profil";
    }
}
