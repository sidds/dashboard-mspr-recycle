package fr.epsi.recyclr.controller;

import fr.epsi.recyclr.config.ApiAccess;
import fr.epsi.recyclr.model.Employee;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String login(Model model, HttpServletRequest request) {
        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        HttpHeaders headers = ApiAccess.setRequestHeader(session);
        System.out.println("le usertoken: "+headers.toString());
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(ApiAccess.getUrl() + "logout", entity, String.class);
        session.invalidate();
        if(responseEntity.getStatusCode() == HttpStatus.OK){
            return "redirect:/login?logout=success";
        }else {
            return "redirect:/login?logout=failed";
        }
    }

    @GetMapping("/reset_session")
    public String resetSession(Model model){
        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        return "reset_session";
    }

    @PostMapping("/reset_session")
    public String resetSession(@ModelAttribute("employee") Employee employee){
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();
        // set `content-type` header
        headers.setContentType(MediaType.APPLICATION_JSON);
        // set `accept` header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        // create a map for post parameters
        Map<String, Object> map = new HashMap<>();
        map.put("login", employee.getData().getLogin());
        map.put("password", employee.getData().getPassword());
        if (map.get("login").equals("") || map.get("password").equals("")) {
            return "redirect:/reset_session?empty_field";
        }
        // build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        // send POST request
        try {
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(ApiAccess.getUrl() + "resetSession", entity, String.class);
            // check response status code
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                System.out.println(responseEntity.getBody());
                return "redirect:/";
            } else {
                System.out.println(responseEntity.getBody());
                return "redirect:/login";
            }
        } catch (RestClientException e) {
            System.out.println("eee = " + e);
            return "redirect:/login?error";
        }
    }

    @GetMapping("/forgot_password")
    public String forgotPassword(Model model) {
        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        return "forgot_password";
    }

    @PostMapping("/forgot")
    public String sendEmail(@ModelAttribute("employee") Employee employee) {
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();
        // set `content-type` header
        headers.setContentType(MediaType.APPLICATION_JSON);
        // set `accept` header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        // create a map for post parameters
        String email = employee.getData().getEmail();
        Map<String, Object> map = new HashMap<>();
        map.put("email", email);
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        try {
            ResponseEntity<Employee> responseEntity = restTemplate.postForEntity(ApiAccess.getUrl() + "forgot", entity, Employee.class);
            // check response status code
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                String successMessage = responseEntity.getBody().getSuccessMessage();
                successMessage = successMessage.substring(0, 1);
                return "redirect:/login";
            } else {
                return "redirect:/login";
            }
        } catch (RestClientException e) {
            System.out.println("e = " + e);
            return "redirect:/login";
        }
    }

    @PostMapping(value = "/authuser")
    public String login(@ModelAttribute("employee") Employee employee, HttpSession session, Model model, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();
        // set `content-type` header
        headers.setContentType(MediaType.APPLICATION_JSON);
        // set `accept` header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        // create a map for post parameters
        Map<String, Object> map = new HashMap<>();
        map.put("login", employee.getData().getLogin());
        map.put("password", employee.getData().getPassword());
        if (map.get("login").equals("") || map.get("password").equals("")) {
            return "redirect:/login?empty_field";
        }
        // build the request
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        // send POST request
        try {
            ResponseEntity<Employee> responseEntity = restTemplate.postForEntity(ApiAccess.getUrl() + "login", entity, Employee.class);
            // check response status code
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                session.setAttribute("employee", responseEntity.getBody());
                System.out.println("Employee connected");
                System.out.println("LA DATA: " + responseEntity.getBody().getData());
                return "redirect:/";
            } else if (responseEntity.getStatusCode() == HttpStatus.NON_AUTHORITATIVE_INFORMATION) {
                String errorMessage = responseEntity.getBody().getErrorMessage();
                errorMessage = errorMessage.substring(0, 1);
                System.out.println("errorMessage = " + errorMessage);
                return "redirect:/login?error=" + errorMessage;
            } else {
                System.out.println("CEST PAS BON");
                return "redirect:/login";
            }
        } catch (RestClientException e) {
            System.out.println("e = " + e);
            return "redirect:/login?error";
        }
    }

    @GetMapping("reset_password")
    public String resetPassword(Model model, HttpSession session, @RequestParam(name = "resettoken") String resetToken) {
        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        session.setAttribute("resetToken", resetToken);
        return "reset_password";
    }

    @PostMapping("reset_password")
    public String sendPasswords(@ModelAttribute("employee") Employee employee, HttpSession session) {
        String resetToken = (String) session.getAttribute("resetToken");
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();
        // set `content-type` header
        headers.setContentType(MediaType.APPLICATION_JSON);
        // set `accept` header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        Map<String, Object> map = new HashMap<>();
        map.put("password1", employee.getPassword1());
        map.put("password2", employee.getPassword2());
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        try{
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(ApiAccess.getUrl() + "reset_password/"+ resetToken, entity, String.class);
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                System.out.println("le body:" + responseEntity.getBody());
            }else {
                System.out.println("le body: "+ responseEntity.getBody());
            }
            return "redirect:/login";
        }catch (HttpClientErrorException e){
            System.out.println("e = " + e);
            return "redirect:/login?error";
        }

    }
}
