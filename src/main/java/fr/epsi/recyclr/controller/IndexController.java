package fr.epsi.recyclr.controller;

import fr.epsi.recyclr.model.*;
import fr.epsi.recyclr.service.EmployeeService;
import fr.epsi.recyclr.service.RequestService;
import fr.epsi.recyclr.service.SiteService;
import fr.epsi.recyclr.service.WasteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@Controller
public class IndexController {
    @Autowired
    WasteService wasteService;
    @Autowired
    RequestService requestService;
    @Autowired
    SiteService siteService;
    @Autowired
    EmployeeService employeeService;


    @RequestMapping("/")
    public String index(HttpSession session, Model model, @RequestParam(required = false) String dateFrom, @RequestParam(required = false) String dateTo, @RequestParam(required = false, defaultValue = "") String site, @RequestParam(required = false, defaultValue = "") String typeDechet) {
        if (dateFrom == null) {
            dateFrom = "01/01/2017";
        }
        if (dateTo == null) {
            LocalDateTime today = LocalDateTime.now();
            DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            dateTo = today.format(myFormatObj);
        }
        Employee employee = (Employee) session.getAttribute("employee");
        System.out.println("LE TOKEN: " + employee.getToken());
        String module = "home";
        session.setAttribute("module", module);
        fillChartWaste(model, session, dateFrom, dateTo, site, typeDechet);
        //Init filter form
        FilterForm dateForm = new FilterForm();
        model.addAttribute("dateForm", dateForm);
//        Get value for number of requests no submit
        int countRequestNoSubmit = requestService.getAllRequestsNoSubmit(session).size();
        model.addAttribute("countRequestNoSubmit", countRequestNoSubmit);
//        Get value of number of waste
        int numberOfWaste = getNumberOfWasteForCurrentMonth(session);
        model.addAttribute("numberOfWaste", numberOfWaste);
        List<String> dateList = getCurrentMonthDate();
        model.addAttribute("dateBegin", dateList.get(0));
        model.addAttribute("dateEnd", dateList.get(1));
//        Get value of number of tour
        List<Data> employeesNbTournee = employeeService.getAllEmployeesNbTournee(session);
        int numberOfTours = 0;
        for (Data detail : employeesNbTournee
        ) {
            numberOfTours += detail.getNbTournee();
        }
        model.addAttribute("numberOfTour", numberOfTours);
//        Get all sites
        List<Site> siteList = siteService.getAllSites(session);
        model.addAttribute("allSites", siteList);
//        Get all types of waste
        List<Waste> wasteList = wasteService.getAllWasteType(session);
        model.addAttribute("allWasteTypes", wasteList);
        return "index";
    }

    private List<String> getCurrentMonthDate(){
        LocalDate date = LocalDate.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//        First day of month
        LocalDate dateBegin = date.withDayOfMonth(1);
        String dateStringBegin = dateBegin.format(myFormatObj);
//        Last day of month
        String dateNowString = date.format(myFormatObj);
        LocalDate convertedDate = LocalDate.parse(dateNowString, myFormatObj);
        convertedDate = convertedDate.withDayOfMonth(
                convertedDate.getMonth().length(convertedDate.isLeapYear()));
        String dateStringEnd = convertedDate.format(myFormatObj);
        List<String> result = new ArrayList<>();
        result.add(dateStringBegin);
        result.add(dateStringEnd);
        return result;
    }

    private int getNumberOfWasteForCurrentMonth(HttpSession session) {
        List<String> dateList = getCurrentMonthDate();
//        Get picked up waste
        List<Waste> wasteList = wasteService.getPickedUpWaste(session, dateList.get(0), dateList.get(1), "", "");
        int numberOfWaste = 0;
//        Calculate number of waste collected
        for (Waste waste : wasteList
        ) {
            numberOfWaste += waste.getQuantiteEnleveee();
        }
        return numberOfWaste;
    }

    private void fillChartWaste(Model model, HttpSession session, String dateFrom, String dateTo, String site, String typeDechet) {
        List<Waste> wasteList = wasteService.getPickedUpWaste(session, dateFrom, dateTo, site, typeDechet);
        model.addAttribute("wasteList", wasteList);
        model.addAttribute("dateFrom", dateFrom);
        model.addAttribute("dateTo", dateTo);
        model.addAttribute("site", site);
        model.addAttribute("typeDechet", typeDechet);
    }

    @PostMapping("updategraph")
    public String updateChart(@ModelAttribute("dateForm") FilterForm filterForm, HttpSession session, Model model) {
        System.out.println("model site: "+ model.getAttribute("allSites"));
        SimpleDateFormat fromForm = new SimpleDateFormat("yyyy-MM");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");
        StringBuilder final_uri = new StringBuilder("redirect:/?");
//        Check input for "typeDechet" filter
        if (!filterForm.getFilterChoice().isEmpty() && filterForm.getFilterChoice().equals("typeDechet")) {
            String noTypeDechet = filterForm.getTypeDechet();
            final_uri.append("typeDechet=").append(noTypeDechet).append("&");
        }
//        Check input for "site" filter
        if (!filterForm.getFilterChoice().isEmpty() && filterForm.getFilterChoice().equals("site") && !filterForm.getSite().isEmpty()) {
            String noSite = filterForm.getSite();
            final_uri.append("site=").append(noSite).append("&");
        }
        try {
            if (!filterForm.getDateFrom().equals("")) {
                String dateFrom = myFormat.format(fromForm.parse(filterForm.getDateFrom()));
                final_uri.append("dateFrom=").append(dateFrom).append("&");
            }
            if (!filterForm.getDateTo().equals("")) {
                String dateTo = myFormat.format(fromForm.parse(filterForm.getDateTo()));
                final_uri.append("dateTo=").append(dateTo).append("&");
            }
            return final_uri.toString();
        } catch (ParseException e) {
            System.out.println("e = " + e);
            return final_uri.toString();
        }

    }

}
