package fr.epsi.recyclr.controller;
import fr.epsi.recyclr.model.DetailRequest;
import fr.epsi.recyclr.model.Employee;
import fr.epsi.recyclr.model.Request;
import fr.epsi.recyclr.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping("/demandes")
public class RequestController {
    @Autowired
    RequestService requestService;

    @RequestMapping("")
    public String requests(Model model, HttpSession session) {
        Employee employee = (Employee) session.getAttribute("employee");
        System.out.println("employee = " + employee.getData());
        List<Request> requests = requestService.getAllRequests(session);
        System.out.println("requests = " + requests);
        model.addAttribute("demandes", requests);
        String module = "requests";
        session.setAttribute("module", module);
        return "demandes";
    }

    @GetMapping("/detail")
    public String detail(HttpServletRequest req, HttpSession session, Model model) {
        String module = "requests";
        session.setAttribute("module", module);
        String param = req.getParameter("id");
        Request request = requestService.getRequest(session, param);
        model.addAttribute("request", request);
        System.out.println("request = " + request);
        return "detail_demande";
    }

    @GetMapping("/unassigned")
    public String unassigned(HttpSession session, Model model){
        String module = "requests";
        session.setAttribute("module", module);
        List<DetailRequest> requestNoSubmitList = requestService.getAllRequestsNoSubmit(session);
        model.addAttribute("requestList", requestNoSubmitList);
        return "demandes_non_affectees";
    }
}
