package fr.epsi.recyclr.controller;

import fr.epsi.recyclr.model.Data;
import fr.epsi.recyclr.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping("/employees")
    public String employees(HttpSession session, Model model) {
        String module = "employees";
        session.setAttribute("module", module);
        List<Data> employeeList = employeeService.getAllEmployeesNbTournee(session);
        model.addAttribute("employeeList", employeeList);
        System.out.println("employeeList = " + employeeList);
        return "employees";
    }

}
