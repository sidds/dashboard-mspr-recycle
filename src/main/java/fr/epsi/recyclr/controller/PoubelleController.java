package fr.epsi.recyclr.controller;

import fr.epsi.recyclr.config.ApiAccess;
import fr.epsi.recyclr.model.TrashCan;
import fr.epsi.recyclr.service.PoubelleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
public class PoubelleController {
    @Autowired
    PoubelleService poubelleService;

    @GetMapping("/poubelles")
    public String getPoubellesPage(HttpSession session, Model model){
        session.setAttribute("module", "poubelles");
        List<TrashCan> trashCanList = poubelleService.getTrashCansInformation(session);
        HashMap<String, List<TrashCan>> hashMap = new HashMap<>();
        for (TrashCan trashCan: trashCanList
             ) {
            String deviceId = trashCan.getDeviceId();
            if (!hashMap.containsKey(deviceId)) {
                List<TrashCan> list = new ArrayList<>();
                list.add(trashCan);
                hashMap.put(deviceId, list);
            } else {
                hashMap.get(deviceId).add(trashCan);
            }
        }
        model.addAttribute("trashCansList", hashMap);
        return "poubelles";
    }

    @PostMapping("/assigner_tournee")
    public String assignTournee(HttpSession session){
        HttpHeaders headers = ApiAccess.setRequestHeader(session);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<String> response = restTemplate.exchange(ApiAccess.getUrl() + "assign-all", HttpMethod.GET, request, String.class);
        System.out.println("response = " + response);
        return "redirect:/";
    }
}
