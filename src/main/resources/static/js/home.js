const ctx = document.getElementById('myChart').getContext('2d');
let labels = [];
let quantities = [];
let colors = [];
for (const request of requestsList) {
    labels.push(request.NOMTYPEDECHET);
    quantities.push(request['SUM("QUANTITEENLEVEE")']);
}
for (let i = 0; i < labels.length; i++) {
    let o = Math.round, r = Math.random, s = 255;
    colors.push('rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + 1 + ')');
}
let myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: labels,
        datasets: [{
            label: 'Types de déchets',
            data: quantities,
            backgroundColor: colors,
            borderColor: colors,
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Nombre de déchets (kg)'
                }
            }]
        }
    }
});
//Change dynamically input for filter choice
const filterChoice = document.getElementById("filterChoice");
const typeDechetLabel = document.getElementById("typeDechet");
const siteLabel = document.getElementById("siteLabel");
const hiddenClass= "hidden";
document.addEventListener("change", ()=>{
    let filterChoiceValue = filterChoice.value;
    if (filterChoiceValue === ""){
        typeDechetLabel.classList.add(hiddenClass);
        siteLabel.classList.add(hiddenClass);
    }
    else if (filterChoiceValue === 'typeDechet'){
        typeDechetLabel.classList.remove(hiddenClass);
        siteLabel.classList.add(hiddenClass);
    }
    else{
        siteLabel.classList.remove(hiddenClass);
        typeDechetLabel.classList.add(hiddenClass);
    }

});