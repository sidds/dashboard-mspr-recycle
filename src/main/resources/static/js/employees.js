$(function () {
    $("#example").DataTable({
        "responsive": true,
        "autoWidth": true,
    });
});
//Filtre moins de N tournées
const rows = document.querySelectorAll('#datatable > tbody > tr');
const nbTourneePicker = document.getElementById("nbTourneePicker");

const resetFilter = () => {
    console.log("on est dans le filter");
    rows.forEach((row, index) => rows[index].style.display = 'table-row');
};

const nbTourneeFinder = () => {
    const nbTourneePickerValue = nbTourneePicker.value;
    if (nbTourneePickerValue.length) {
        rows.forEach((row, index) => {
            const nbTournee = row.querySelector('.row-nbtournee').textContent;
            if(parseInt(nbTournee) < parseInt(nbTourneePickerValue)){
                rows[index].style.display = 'table-row';
            }else {
                rows[index].style.display = 'none';
            }
        });
    } else {
        resetFilter();
    }
};
nbTourneePicker.addEventListener('change', nbTourneeFinder);