let graph = document.getElementById('myChart').getContext('2d');
let graph2 = document.getElementById('myChart2').getContext('2d');

let dataFillLevel = []
let labels = []
for (const resultKey in trashcansList) {
    let currentArray = trashcansList[resultKey];
    dataFillLevel = []
    labels = []
    for (let object of currentArray) {
        dataFillLevel.push(object["FILLLEVEL"]);
        let date = new Date(parseInt(object["DATE_RELEVE"]));
        let newDateFormat = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours()).toUTCString();
        labels.push(newDateFormat);
    }
}

let chart = new Chart(graph, {
    // The type of chart we want to create
    type: 'line',
    // The data for our dataset
    data: {
        labels: labels,
        datasets: [{
            label: 'Niveau de remplissage',
            borderColor: 'rgb(255, 99, 132)',
            data: dataFillLevel
        }]
    },
    // Configuration options go here
    options: {
        scales: {
            xAxes: [{
                ticks: {
                    display: false //this will remove only the label
                }
            }]
        }
    }
});


