const rows = document.querySelectorAll('#datatable > tbody > tr');
const datePicker = document.getElementById('datepicker_from');
const noDemandePicker = document.getElementById('noDemandePicker');

const resetDateFilter = () => {
    rows.forEach((row, index) => rows[index].style.display = 'table-row');
};
const resetIdFilter = () => {
    rows.forEach((row, index) => rows[index].style.display = 'table-row');
};

const dateFinder = () => {
    const datePickerValue = datePicker.value;
    if (datePickerValue.length) {
        const datePickerValueTS = Date.parse(new Date(datePickerValue));
        rows.forEach((row, index) => {
            const date = row.querySelector('.row-date').textContent;
            const dateTS = Date.parse(new Date(date));
            if (dateTS < datePickerValueTS) {
                rows[index].style.display = 'none';
            } else {
                rows[index].style.display = 'table-row';
            }
        });
    } else {
        resetDateFilter();
    }
};

function sortIdRequest() {
    const noDemandePickerValue = noDemandePicker.value;
    if (noDemandePickerValue.length) {
        rows.forEach((row, index) => {
            const id = row.querySelector('.row-id').textContent;
            if (id !== noDemandePickerValue) {
                rows[index].style.display = 'none';
            } else {
                rows[index].style.display = 'table-row';
            }
        });
    } else {
        resetIdFilter();
    }
}

datePicker.addEventListener('change', dateFinder);
noDemandePicker.addEventListener('change', sortIdRequest);
